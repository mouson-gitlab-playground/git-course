# git-course

## 一、Git 入門觀念到實作

* 範例下載：

透過 [Git and GitLab Course](https://gitlab.com/mouson-gitlab-playground/git-course) 這個專案，進入 Repository 選擇 Tags 可以看到目前的版本，可下載 Example.zip

![Download Example File](./images/DownloadExample.png)

* 設定 comder 的 git 環境，不管換行符號：

```sh
git config --global core.autocrlf false
```

* 確認 autocrlf 設定為 false：

```sh
git config --get core.autocrlf
```

![Git Core Autocrlf False](./images/git_core_autocrlf_false.png)

* 將範例載入 SourceTree 可以用拖拉的方式：

![Project Can drop to SourceTree](./images/ExampleImportToSourceTree.gif)

### commit ref

1. 將標題與內容中間多一行空白

2. 標題限制 50 字元
3. 標題第一個字必須為大寫
4. 標題最後不要帶上句號
5. 標題內容可以使用強烈的語氣
6. 內容請用 72 字元來斷行
7. 內容可以解釋 what and why vs. how

### REF:

1. [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) - https://chris.beams.io/posts/git-commit/
2. [Conventional Commits](https://www.conventionalcommits.org/) - https://www.conventionalcommits.org/

## 二、Git 流程管理

1. [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) - https://nvie.com/posts/a-successful-git-branching-model/
2. [Understanding the GitHub flow](https://guides.github.com/introduction/flow/) - https://guides.github.com/introduction/flow/
3. [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/) - https://about.gitlab.com/2014/09/29/gitlab-flow/
4. [The 11 Rules of GitLab Flow](https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/) - https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/

## 三、GitLab 入門及使用

### 1. Demo Example Setting:

* 下載環境
```
git clone git@gitlab.com:mouson-gitlab-playground/gitlab-cicd-demo.git sample.demo.ol

```

* 安裝 python 基本需求
```sh
cd sample.demo.ol
python3 -m venv virtualenv
./virtualenv/bin/pip3 install -r requirements.txt
./virtualenv/bin/python3 manage.py migrate
./virtualenv/bin/python3 manage.py collectstatic --noinput
```

* 設定環境變數
```sh
cp .env.sample .env
vim .env
```

* 執行單元測試
```sh
./virtualenv/bin/python3 manage.py test accounts lists
```

* 本地執行
```sh
./virtualenv/bin/python3 manage.py runserver
```

* 執行 E2E 測試
```
./virtualenv/bin/pip3 install selenium
./virtualenv/bin/pip3 install fabric3
./virtualenv/bin/python3 manage.py test functional_tests
```

### 2. 建立 CICD

* 每次的 commit push 都需要測試

```yml
stages:
  - test

python-test:3.6:
  image: python:3.6
  tags:
    - docker
  stage: test
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python3 --version
    - python3 manage.py test accounts lists
```

* 增加 python 3.7 版本測試

```yml
python-test:3.7:
  image: python:3.7
  stage: test
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python3 --version
    - python3 manage.py test accounts lists
```

* 取得 code coverage 數字，並且顯示

```yml
    - python3 manage.py test accounts lists
  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+%)/'
```

```yml
  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+%)/'
```

* 產生報表顯示並推送到 gitlab 提供的 pages 上

```yml
    - python3 manage.py test accounts lists --cover-html-dir=reports --cover-html
  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+%)/'
  artifacts:
    paths:
      - reports/
```

新增 delpoy stage

```yml
stages:
  - test
  - deploy
```

```yml
pages:
  stage: deploy
  tags:
    - docker
  dependencies:
    - python-test:3.6
  script:
    - mv reports public
    - cat ./public/index.html
  artifacts:
    paths:
      - public
  only:
    - master
```

ref: https://mouson-gitlab-playground.gitlab.io/gitlab-cicd-demo/

* 測試成功正式回到 master 後，收集每次的成果為 artifact

1. 增加 build stage

```yml
stages:
  - test
  - build
  - deploy
```

2. 新增 ./deploy_tools/artifact_list.txt

```yml
accounts
deploy_tools
lists
static
superlists
functional_tests
virtualenv
LICENSE
manage.py
README.md
requirements.txt
.env.sample
```

3. 建立 build task

```yml
build-artifact:3.6:
  stage: build
  image: python:3.6
  tags:
    - docker
  dependencies:
    - python-test:3.6
  before_script:
    - python3 -m venv virtualenv
  script:
    - ls -al ./virtualenv/bin/
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py collectstatic --noinput
    - chmod -R a+rX,og-w .
    - tar -pcvzf release.tar.gz -T ./deploy_tools/artifact_list.txt
  artifacts:
    paths:
      - release.tar.gz
    expire_in: 30 days
  only:
    - master
```

4. 修改為變數之後

```yml
build-artifact:3.6:
  stage: build
  image: python:3.6
  tags:
    - docker
  dependencies:
    - python-test:3.6
  variables:
    RELEASE_NAME: release.tar.gz
  before_script:
    - python3 -m venv virtualenv
  script:
    - ls -al ./virtualenv/bin/
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py collectstatic --noinput
    - chmod -R a+rX,og-w .
    - tar -pcvzf ${RELEASE_NAME} -T ./deploy_tools/artifact_list.txt
  artifacts:
    paths:
      - ${RELEASE_NAME}
    expire_in: 30 days
  only:
    - master
```

* 部署到 staging 環境

1. 先在 gitlab 上建立 environment 環境變數

TARGET_DOMAIN => staging.demo.ol for staging env


2. 設定整體之環境變數

```yml
deploy-to-staging:
  variables:
    GIT_STRATEGY: none
    RELEASE_NAME: release.tar.gz
  tags:
    - ssh
  environment:
    name: staging
  dependencies:
    - build-artifact:3.6
  stage: deploy
  script:
    - whoami
    - pwd
    - ls -al
    - TMP_FOLDER=~/sites/${TARGET_DOMAIN}_Tmp
    - TARGET=~/sites/${TARGET_DOMAIN}
    - mkdir -p ${TMP_FOLDER}
    - mkdir -p ${TARGET}
    - mv ${RELEASE_NAME} ${TMP_FOLDER}
    - cd ${TMP_FOLDER}
    - tar -zxvf ${RELEASE_NAME}
    - rm -f ${RELEASE_NAME}
    - cd ${TARGET}
    - xargs rm -r < ${TMP_FOLDER}/deploy_tools/artifact_list.txt || true
    - mv ${TMP_FOLDER}/* ${TARGET}/
    - rm -rf ./virtualenv
    - python3 -m venv virtualenv
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py migrate
    - sudo systemctl restart ${TARGET_DOMAIN}
    - rm -rf ${TMP_FOLDER}
  only:
    - master
  when: manual
```

* 當下 tag 的時候部署到 production

1. 先在 gitlab 上建立 environment 環境變數

TARGET_DOMAIN => production.demo.ol for production env

2. 補 build 階段的 only 加上 tags

```yml
  only:
    - tags
    - master
```

3. 複製程式碼

```yml
deploy-to-production:
  variables:
    GIT_STRATEGY: none
    RELEASE_NAME: release.tar.gz
  tags:
    - ssh
  environment:
    name: production
  dependencies:
    - build-artifact:3.6
  stage: deploy
  script:
    - whoami
    - pwd
    - ls -al
    - TMP_FOLDER=~/sites/${TARGET_DOMAIN}_Tmp
    - TARGET=~/sites/${TARGET_DOMAIN}
    - mkdir -p ${TMP_FOLDER}
    - mkdir -p ${TARGET}
    - mv ${RELEASE_NAME} ${TMP_FOLDER}
    - cd ${TMP_FOLDER}
    - tar -zxvf ${RELEASE_NAME}
    - rm -f ${RELEASE_NAME}
    - cd ${TARGET}
    - xargs rm -r < ${TMP_FOLDER}/deploy_tools/artifact_list.txt || true
    - mv ${TMP_FOLDER}/* ${TARGET}/
    - rm -rf ./virtualenv
    - python3 -m venv virtualenv
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py migrate
    - sudo systemctl restart ${TARGET_DOMAIN}
    - rm -rf ${TMP_FOLDER}
  only:
    - tags
  when: manual
```

* 完成版本程式碼：

```yml
stages:
  - test
  - build
  - deploy

python-test:3.6:
  image: python:3.6
  tags:
    - docker
  stage: test
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python3 --version
    - python3 manage.py test accounts lists --cover-html-dir=reports --cover-html
  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+%)/'
  artifacts:
    paths:
      - reports/

python-test:3.7:
  image: python:3.7
  stage: test
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python3 --version
    - python3 manage.py test accounts lists
  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+%)/'

build-artifact:3.6:
  stage: build
  image: python:3.6
  tags:
    - docker
  dependencies:
    - python-test:3.6
  variables:
    RELEASE_NAME: release.tar.gz
  before_script:
    - python3 -m venv virtualenv
  script:
    - ls -al ./virtualenv/bin/
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py collectstatic --noinput
    - chmod -R a+rX,og-w .
    - tar -pcvzf ${RELEASE_NAME} -T ./deploy_tools/artifact_list.txt
  artifacts:
    paths:
      - ${RELEASE_NAME}
    expire_in: 30 days
  only:
    - tags
    - master

deploy-to-staging:
  variables:
    GIT_STRATEGY: none
    RELEASE_NAME: release.tar.gz
  tags:
    - ssh
  environment:
    name: staging
  dependencies:
    - build-artifact:3.6
  stage: deploy
  script:
    - whoami
    - pwd
    - ls -al
    - TMP_FOLDER=~/sites/${TARGET_DOMAIN}_Tmp
    - TARGET=~/sites/${TARGET_DOMAIN}
    - mkdir -p ${TMP_FOLDER}
    - mkdir -p ${TARGET}
    - mv ${RELEASE_NAME} ${TMP_FOLDER}
    - cd ${TMP_FOLDER}
    - tar -zxvf ${RELEASE_NAME}
    - rm -f ${RELEASE_NAME}
    - cd ${TARGET}
    - xargs rm -r < ${TMP_FOLDER}/deploy_tools/artifact_list.txt || true
    - mv ${TMP_FOLDER}/* ${TARGET}/
    - rm -rf ./virtualenv
    - python3 -m venv virtualenv
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py migrate
    - sudo systemctl restart ${TARGET_DOMAIN}
    - rm -rf ${TMP_FOLDER}
  only:
    - master
  when: manual

deploy-to-production:
  variables:
    GIT_STRATEGY: none
    RELEASE_NAME: release.tar.gz
  tags:
    - ssh
  environment:
    name: production
  dependencies:
    - build-artifact:3.6
  stage: deploy
  script:
    - whoami
    - pwd
    - ls -al
    - TMP_FOLDER=~/sites/${TARGET_DOMAIN}_Tmp
    - TARGET=~/sites/${TARGET_DOMAIN}
    - mkdir -p ${TMP_FOLDER}
    - mkdir -p ${TARGET}
    - mv ${RELEASE_NAME} ${TMP_FOLDER}
    - cd ${TMP_FOLDER}
    - tar -zxvf ${RELEASE_NAME}
    - rm -f ${RELEASE_NAME}
    - cd ${TARGET}
    - xargs rm -r < ${TMP_FOLDER}/deploy_tools/artifact_list.txt || true
    - mv ${TMP_FOLDER}/* ${TARGET}/
    - rm -rf ./virtualenv
    - python3 -m venv virtualenv
    - ./virtualenv/bin/pip3 install -r requirements.txt
    - ./virtualenv/bin/python3 manage.py migrate
    - sudo systemctl restart ${TARGET_DOMAIN}
    - rm -rf ${TMP_FOLDER}
  only:
    - tags
  when: manual

pages:
  stage: deploy
  tags:
    - docker
  dependencies:
    - python-test:3.6
  script:
    - mv reports public
    - cat ./public/index.html
  artifacts:
    paths:
      - public
  only:
    - master
```

### Ref:

1. [Test-Driven Development with Python](https://www.obeythetestinggoat.com/) - https://www.obeythetestinggoat.com/
2. [Test-Driven Development with Python Book Example Source Code](https://github.com/hjwp/book-example) - https://github.com/hjwp/book-example
3. [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/README.html) - https://docs.gitlab.com/ee/ci/yaml/README.html